<?php

declare(strict_types=1);

namespace Benchmarks\Craynic\AhoCorasick\SearchTree;

use Craynic\AhoCorasick\Dictionary\Dictionary;
use Craynic\AhoCorasick\Dictionary\StaticDictionary;
use Craynic\AhoCorasick\SearchTree\SearchTree;

abstract class BenchmarkBase
{
    private const NEEDLE = 'test_the_best';
    private const NEEDLE_PREFIX_LENGTH = 10000;
    private const NEEDLE_SUFFIX_LENGTH = 10000;
    private const DICTIONARY_SIZE = 10000;
    private const DICTIONARY_WORD_LENGTH_MIN = 4;
    private const DICTIONARY_WORD_LENGTH_MAX = 30;

    protected Dictionary $dictionary;
    protected string $searchedText;
    private ?SearchTree $searchTree = null;

    public function init(): void
    {
        $this->dictionary = $this->initDictionary();
        $this->searchedText = $this->initSearchedText();
    }

    private function initDictionary(): StaticDictionary
    {
        $dictionary = [self::NEEDLE];

        for ($offset = 0; $offset < self::DICTIONARY_SIZE; $offset++) {
            $dictionary[] = $this->generateRandomString(
                mt_rand(self::DICTIONARY_WORD_LENGTH_MIN, self::DICTIONARY_WORD_LENGTH_MAX)
            );
        }

        return new StaticDictionary($dictionary);
    }

    private function initSearchedText(): string
    {
        return $this->generateRandomString(self::NEEDLE_PREFIX_LENGTH)
            . self::NEEDLE
            . $this->generateRandomString(self::NEEDLE_SUFFIX_LENGTH);

    }

    private function generateRandomString(int $length): string
    {
        $spool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $nonRandomString = str_repeat($spool, (int) ceil($length / strlen($spool)));
        $randomString = str_shuffle($nonRandomString);

        return substr($randomString, 0, $length);
    }

    public function initBench(): void
    {
        $this->init();

        $this->searchTree = $this->getSearchTree($this->dictionary);
    }

    abstract protected function getSearchTree(Dictionary $dictionary): SearchTree;

    /**
     * @BeforeMethods({"initBench"})
     * @Revs(10)
     * @Iterations(10)
     */
    public function bench(): void
    {
        $result = $this->searchTree->search($this->searchedText);

        iterator_to_array($result);
    }

    /**
     * @BeforeMethods({"init"})
     * @Iterations(10)
     */
    public function benchNoWarmup(): void
    {
        $result = $this->getSearchTree($this->dictionary)->search($this->searchedText);

        iterator_to_array($result);
    }
}
