<?php

declare(strict_types=1);

namespace Benchmarks\Craynic\AhoCorasick\SearchTree;

use Craynic\AhoCorasick\Dictionary\Dictionary;
use Craynic\AhoCorasick\SearchTree\AhoCorasickSearchTree;
use Craynic\AhoCorasick\SearchTree\SearchTree;
use Craynic\AhoCorasick\Utf8Iterator\NaiveUtf8Iterator;

final class AhoCorasickSearchTreeBenchmark extends BenchmarkBase
{
    protected function getSearchTree(Dictionary $dictionary): SearchTree
    {
        $searchTree = new AhoCorasickSearchTree(new NaiveUtf8Iterator());
        $searchTree->setDictionary($dictionary);

        return $searchTree;
    }
}
