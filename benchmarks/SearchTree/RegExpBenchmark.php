<?php

declare(strict_types=1);

namespace Benchmarks\Craynic\AhoCorasick\SearchTree;

use Craynic\AhoCorasick\Dictionary\Dictionary;
use Craynic\AhoCorasick\SearchTree\RegExpSearchTree;
use Craynic\AhoCorasick\SearchTree\SearchTree;

final class RegExpBenchmark extends BenchmarkBase
{
    protected function getSearchTree(Dictionary $dictionary): SearchTree
    {
        $searchTree = new RegExpSearchTree();
        $searchTree->setDictionary($dictionary);

        return $searchTree;
    }
}
