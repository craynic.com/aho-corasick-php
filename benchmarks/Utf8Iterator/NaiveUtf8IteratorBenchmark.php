<?php

declare(strict_types=1);

namespace Benchmarks\Craynic\AhoCorasick\Utf8Iterator;

use Craynic\AhoCorasick\Utf8Iterator\NaiveUtf8Iterator;
use PHPUnit\Framework\Assert;

final class NaiveUtf8IteratorBenchmark
{
    private const TEXT = 'Zakobouch raze lišví odyž ož te sibýta surí zaš. Hokle zvotěhlo žarosvitka drůde křebeštil
troh blouták dář korsí podž. Bluchlík faje pašputánek bloumeš makrodělna musevnost. Daš ostrohlubník kutylich zad.
Luchní opně bavihrajky česrdník vydeluš uš bludka c tysene oloboš. Mjí kafiší pněle ek klochopta mošavuch odeš ac
tabrakráť zářihojka. Kobřadlice lapiší. Zu ceze žute ačně ličba otlo velerobna požt. Voštalán lapišně kže šivíř
strašibuch labrták těhovec dul hocutrol veň. Podaž zípe raze vozokolník cojš upěte nazaš vrne. Šlepr bubelaštík. Ič
šenoce laboretka un vni letěšbel du še vežec vuč. Čertolez lbeň upětačil blumbeří iž lutěkul klehotní duká rašpublitán
fosantr. Drahopučka dlakoruch beroutle oně. Promělka ufce du svajde tubolant hřeste. Eč vnuž uk lutiví šlobichule
rachtadník ož vučeklář vec rafkusák. Repš povlakoň šponcovna pídrhaš požt šinšprdlík oktes rátlo tykokuj břekvobuň.
Podukál kalažmitlík eč repše flejpa notablin přetemec ac tapycír ounodek. Rdybeš hvíždil zukazba hoštiper kalažme
sanokobec cincifáš pahordí zuče hosnice. Svě vrtososák vyblacák n cabřanát rachobušně. Kolobutev hašornet usomodec itež.
Perkulíno zdye spojbodnice vošite botant úz e hlerník djí litěšpich. Jitadlí tuď e kaselník šešoňka veletoř un ukrtálek
štepoluch toršemín. Ež kandoškál. Hlíbec os ac tosametle dromosnice zněčice vévajda svoží. Un mráteník oš bajoblík
n čiptán znad kutle kořeblík tyžec. Vrpice h vráve še bumbibáš e xehožole zaldař. H šebá. Vydelí tuď ak vepratník mleze
jna ubelda sjim pěze svorholec. Turcovák merdyšle at ejž ouzá iž ož úz iž zalde. Ustavášek osyšun okurantál kumahelník.
Vošitace kobabkun opanárna myšticvakna potochýl daš. Jahleso tyže množitace lnive ratumel myšiklikna d tosle kulmešně
šindobe. Okře mrek trachutaj c navže omrumelec čívipretka vechtevář vertoč zasíkočel. Rahuán levihoun h lumofon trobedej
ambruminášek horbe krasolibnost karbašip truví. Okrží podež roukezák šimodrň nalajmice chvádivec raští čně vuká utě.
Pětí tavirudle veneš drtíže ždok veň.';

    /**
     * @Revs(1000)
     * @Iterations(10)
     */
    public function benchNaiveIterator(): void
    {
        $iterator = new NaiveUtf8Iterator();

        $chars = 0;
        foreach ($iterator->iterate(self::TEXT) as $letter) {
            $chars++;
        }

        Assert::assertEquals(2000, $chars);
    }

    /**
     * @Revs(1000)
     * @Iterations(10)
     */
    public function benchPregSplit(): void
    {
        $chars = count(preg_split('/(?<!^)(?!$)/u', self::TEXT));

        Assert::assertEquals(2000, $chars);
    }
}
