<?php

declare(strict_types=1);

namespace Craynic\AhoCorasick\SearchTree;

use Craynic\AhoCorasick\Dictionary\Dictionary;
use Generator;
use RuntimeException;

final class RegExpSearchTree implements SearchTree
{
    private ?Dictionary $dictionary = null;

    public function setDictionary(Dictionary $dictionary): void
    {
        $this->dictionary = $dictionary;
    }

    public function search(string $text): Generator
    {
        if ($this->dictionary === null) {
            throw new RuntimeException('The dictionary is unset.');
        }

        $regexps = array_map(
            function (string $word): string {
                return '|^(.*)' . preg_quote($word) . '|';
            },
            iterator_to_array($this->dictionary)
        );

        foreach ($regexps as $key => $regexp) {
            $matches = [];
            if (preg_match($regexp, $text, $matches)) {
                $dictionaryItem = $this->dictionary->getByKey($key);

                yield new MatchToken(
                    $key,
                    strlen($matches[1]),
                    strlen($matches[1]) + strlen($dictionaryItem) - 1,
                    mb_strlen($matches[1]),
                    mb_strlen($matches[1]) + mb_strlen($dictionaryItem) - 1

                );
            }
        }
    }
}
