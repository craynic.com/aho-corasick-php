<?php

declare(strict_types=1);

namespace Craynic\AhoCorasick\SearchTree;

use Craynic\AhoCorasick\Dictionary\Dictionary;
use Generator;

interface SearchTree
{
    public function setDictionary(Dictionary $dictionary): void;

    public function search(string $text): Generator;
}
