<?php

declare(strict_types=1);

namespace Craynic\AhoCorasick\SearchTree;

use Craynic\AhoCorasick\Dictionary\Dictionary;
use Craynic\AhoCorasick\Utf8Iterator\Utf8Iterator;
use Generator;
use RuntimeException;
use SplDoublyLinkedList;
use SplFixedArray;
use SplQueue;
use function count;

/**
 * Aho-Corasick builds a state tree for a dictionary where each state has attached a set of matches to it.
 * Every state can be achieved through a "goto" transition from a previous state and a letter; if there's no state
 * for a state/letter combination, Aho-Corasick uses a "fail" transition to fall back to a different state.
 * After the tree has been built, Aho-Corasick walks through the letters of searched text just once, transitioning
 * through the states of the tree and reporting all the matches associated to the state.
 *
 * The dictionary is given as a [out => string] pair, where "out" is an identifier of the match and "string" is the
 * text being searched. If the same string is given twice with different "out" value, it will be reported twice.
 * "out" can be the string itself, that is using [string => string] array.
 */
final class AhoCorasickSearchTree implements SearchTree
{
    public const IGNORE_CASE = 1;

    private Utf8Iterator $utf8Iterator;
    private int $flags;
    private ?Dictionary $dictionary = null;
    private SplFixedArray $outKeys;
    private SplFixedArray $outData;
    private SplFixedArray $goto;
    private ?SplFixedArray $fail;

    public function __construct(Utf8Iterator $utf8Iterator, int $flags = 0)
    {
        $this->utf8Iterator = $utf8Iterator;
        $this->flags = $flags;

        $this->reset();
    }

    private function reset(): void
    {
        $this->outKeys = new SplFixedArray(1);
        $this->outData = new SplFixedArray(1);
        $this->goto = new SplFixedArray(1);
        $this->fail = null;
    }

    public function setDictionary(Dictionary $dictionary): void
    {
        $this->reset();

        $this->dictionary = $dictionary;

        $this->build();
    }

    private function build(): void
    {
        $outDataInitialCount = count($this->outData);
        $this->outData->setSize($outDataInitialCount + count($this->dictionary));
        $counter = 0;

        foreach ($this->dictionary as $key => $dictionaryItem) {
            $normalizedDictionaryItem = $this->isCaseSensitive()
                ? $dictionaryItem
                : $this->lowercaseWord($dictionaryItem);

            // create new outDataKey
            $outDataKey = $outDataInitialCount + $counter++;
            $this->outData[$outDataKey] = $key;

            // get the end state for the dictionary item & register the new outDataKey for the state
            $this->setOutDataKeyForState(
                $this->getStateForNewDictionaryItem($normalizedDictionaryItem),
                $outDataKey
            );
        }

        $this->buildFailFunction();
    }

    private function setOutDataKeyForState(int $state, int $outDataKey): void
    {
        if (!isset($this->outKeys[$state])) {
            $this->outKeys[$state] = SplFixedArray::fromArray([$outDataKey]);
            return;
        }

        // $this->outKeys is SplFixedArray[SplFixedArray] - we have to use concrete keys!
        $newKey = $this->outKeys[$state]->getSize();
        $this->outKeys[$state]->setSize($newKey + 1);
        $this->outKeys[$state][$newKey] = $outDataKey;
    }

    /**
     * Searches through a text and returns all matches as an array of [position => out]. Since Generators are used,
     * multiple "outs" can be returned for the same position! Positions are 0-based.
     *
     * @param string $text
     * @return Generator Generator yielding [0-based position => out] pairs; same positions can return multiple times
     */
    public function search(string $text): Generator
    {
        if ($this->dictionary === null) {
            throw new RuntimeException('The dictionary is unset.');
        }

        $filteredText = $this->isCaseSensitive()
            ? $text
            : $this->lowercaseWord($text);

        $state = 0;
        $bytePos = 0;
        $charPos = 0;

        foreach ($this->utf8Iterator->iterate($filteredText) as $char) {
            $state = $this->findNextState($state, $char);

            // this is O(1) in PHP
            $charPos++;
            $bytePos += strlen($char);

            if (isset($this->outKeys[$state])) {
                foreach ($this->outKeys[$state] as $foundOutKey) {
                    $out = $this->outData[$foundOutKey];
                    $dictionaryItem = $this->dictionary->getByKey($out);

                    yield new MatchToken(
                        $out,
                        $bytePos - strlen($dictionaryItem),
                        $bytePos - 1,
                        $charPos - mb_strlen($dictionaryItem),
                        $charPos - 1
                    );
                }
            }
        }
    }

    private function findNextState(int $state, string $char): int
    {
        while ($state !== 0 && !isset($this->goto[$state][$char])) {
            $state = $this->fail[$state] ?? 0;
        }

        return $this->goto[$state][$char] ?? 0;
    }

    private function buildFailFunction(): void
    {
        $this->fail = new SplFixedArray($this->goto->getSize());
        $queue = new SplQueue();
        $queue->setIteratorMode(SplDoublyLinkedList::IT_MODE_DELETE);

        if (isset($this->goto[0])) {
            foreach ($this->goto[0] as $state) {
                $queue->enqueue($state);
            }
        }

        foreach ($queue as $stateFromQueue) {
            if (isset($this->goto[$stateFromQueue])) {
                foreach ($this->goto[$stateFromQueue] as $letter => $state) {
                    $queue->enqueue($state);

                    $failState = $this->findNextState($this->fail[$stateFromQueue] ?? 0, (string) $letter);

                    $this->fail[$state] = $failState;

                    if (isset($this->outKeys[$failState])) {
                        $this->addOutKeysToStateFromFailState($state, $failState);
                    }
                }
            }
        }
    }

    private function addOutKeysToStateFromFailState(int $state, int $failState): void
    {
        // create or extend the outKeys for the state
        if (!isset($this->outKeys[$state])) {
            $this->outKeys[$state] = new SplFixedArray($this->outKeys[$failState]->getSize());
            $offset = 0;
        } else {
            $offset = $this->outKeys[$state]->getSize();
            $this->outKeys[$state]->setSize($offset + $this->outKeys[$failState]->getSize());
        }

        // copy all outKeys from the failstate to our state
        foreach ($this->outKeys[$failState] as $newState) {
            $this->outKeys[$state][$offset++] = $newState;
        }
    }

    private function getStateForNewDictionaryItem(string $word): int
    {
        $state = 0;

        foreach ($this->utf8Iterator->iterate($word) as $letter) {
            if (isset($this->goto[$state][$letter])) {
                $state = $this->goto[$state][$letter];
                continue;
            }

            $newState = $this->createNewState();
            $this->addGotoForLetter($letter, $state, $newState);
            $state = $newState;
        }

        return $state;
    }

    private function createNewState(): int
    {
        $newState = count($this->goto);
        $this->goto->setSize($newState + 1);
        $this->outKeys->setSize($newState + 1);

        return $newState;
    }

    private function addGotoForLetter(string $letter, int $fromState, int $toState): void
    {
        // $this->goto is SplFixedArray! that's why we have to do this charade
        $gotosForState = $this->goto[$fromState] ?? [];
        $gotosForState[$letter] = $toState;
        $this->goto[$fromState] = $gotosForState;
    }

    private function isCaseSensitive(): bool
    {
        return ($this->flags & self::IGNORE_CASE) === 0;
    }

    private function lowercaseWord(string $word): string
    {
        return mb_strtolower($word, 'UTF-8');
    }
}
