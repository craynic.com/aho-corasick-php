<?php

declare(strict_types=1);

namespace Craynic\AhoCorasick\SearchTree;

final class MatchToken
{
    private mixed $matchKey;
    private int $startBytePos;
    private int $endBytePos;
    private int $startCharPos;
    private int $endCharPos;

    public function __construct(
        $matchKey,
        int $startBytePos,
        int $endBytePos,
        int $startCharPos,
        int $endCharPos
    ) {
        $this->matchKey = $matchKey;
        $this->startBytePos = $startBytePos;
        $this->endBytePos = $endBytePos;
        $this->startCharPos = $startCharPos;
        $this->endCharPos = $endCharPos;
    }

    public function getMatchKey()
    {
        return $this->matchKey;
    }

    public function getStartBytePos(): int
    {
        return $this->startBytePos;
    }

    public function getEndBytePos(): int
    {
        return $this->endBytePos;
    }

    public function getMatchBytesLength(): int
    {
        return $this->getEndBytePos() - $this->getStartBytePos() + 1;
    }

    public function getStartCharPos(): int
    {
        return $this->startCharPos;
    }

    public function getEndCharPos(): int
    {
        return $this->endCharPos;
    }
}
