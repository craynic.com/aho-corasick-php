<?php

declare(strict_types=1);

namespace Craynic\AhoCorasick\Utf8Iterator;

use Generator;
use RuntimeException;

final class NaiveUtf8Iterator implements Utf8Iterator
{
    public function iterate(string $utf8String): Generator
    {
        $utf8StringLength = strlen($utf8String);
        $currentBytePos = 0;
        $currentCharPos = 0;

        while ($currentBytePos < $utf8StringLength) {
            $currentByte = ord($utf8String[$currentBytePos]);

            if ($currentByte <= 127) {
                $currentChar = $utf8String[$currentBytePos++];
            } else {
                if ($currentByte >= 192 && $currentByte <= 223) {
                    $currentCharLength = 2;
                } elseif ($currentByte >= 224 && $currentByte <= 239) {
                    $currentCharLength = 3;
                } elseif ($currentByte >= 240 && $currentByte <= 247) {
                    $currentCharLength = 4;
                } else {
                    throw new RuntimeException('Invalid UTF-8 string.');
                }

                $currentChar = substr($utf8String, $currentBytePos, $currentCharLength);
                $currentBytePos += $currentCharLength;
            }

            yield $currentCharPos++ => $currentChar;
        }
    }
}
