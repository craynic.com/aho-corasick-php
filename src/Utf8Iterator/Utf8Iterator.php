<?php

declare(strict_types=1);

namespace Craynic\AhoCorasick\Utf8Iterator;

use Generator;

interface Utf8Iterator
{
    public function iterate(string $utf8String): Generator;
}
