<?php

declare(strict_types=1);

namespace Craynic\AhoCorasick;

use Generator;
use Craynic\AhoCorasick\Dictionary\Dictionary;
use Craynic\AhoCorasick\SearchTree\MatchToken;
use Craynic\AhoCorasick\SearchTree\SearchTree;

final class Replacer
{
    private SearchTree $searchTree;

    public function __construct(SearchTree $searchTree)
    {
        $this->searchTree = $searchTree;
    }

    public function replace(string $text, Dictionary $replaces): string
    {
        $newText = $text;
        $accumulatedOffset = 0;

        /** @var MatchToken $match */
        foreach ($this->getNonOverlappingMatches($this->searchTree->search($text)) as $match) {
            $realStartBytePos = $match->getStartBytePos() + $accumulatedOffset;
            $realEndBytePos = $match->getEndBytePos() + $accumulatedOffset;

            $replace = $replaces->getByKey($match->getMatchKey());
            $byteDiff = strlen($replace) - $match->getMatchBytesLength();

            $newText = sprintf(
                '%1$s%2$s%3$s',
                substr($newText, 0, $realStartBytePos),
                $replace,
                substr($newText, $realEndBytePos + 1)
            );

            $accumulatedOffset += $byteDiff;
        }

        return $newText;
    }

    private function getNonOverlappingMatches(Generator $searchResult): Generator
    {
        /** @var MatchToken[] $matches */
        $matches = iterator_to_array($searchResult);

        // sort by the occurence & found text
        $this->sortMatches($matches);

        $limitPos = 0;

        foreach ($matches as $match) {
            if ($match->getStartBytePos() >= $limitPos) {
                yield $match;
                $limitPos = $match->getEndBytePos() + 1;
            }
        }
    }

    /**
     * @param MatchToken[] $matches
     */
    private function sortMatches(array &$matches): void
    {
        usort(
            $matches,
            function (MatchToken $matchA, MatchToken $matchB): int {
                return $this->sortCallback($matchA, $matchB);
            }
        );
    }

    private function sortCallback(MatchToken $matchA, MatchToken $matchB): int
    {
        if ($matchA->getStartBytePos() < $matchB->getStartBytePos()) {
            return -1;
        } elseif ($matchA->getStartBytePos() > $matchB->getStartBytePos()) {
            return 1;
        }

        if ($matchA->getEndBytePos() < $matchB->getEndBytePos()) {
            return -1;
        } elseif ($matchA->getEndBytePos() > $matchB->getEndBytePos()) {
            return 1;
        }

        return 0;
    }
}
