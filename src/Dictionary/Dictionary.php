<?php

declare(strict_types=1);

namespace Craynic\AhoCorasick\Dictionary;

use Countable;
use Traversable;

interface Dictionary extends Traversable, Countable
{
    public function getByKey($key): string;
}
