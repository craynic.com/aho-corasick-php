<?php

declare(strict_types=1);

namespace Craynic\AhoCorasick\Dictionary;

use ArrayIterator;
use IteratorAggregate;

final class StaticDictionary implements Dictionary, IteratorAggregate
{
    private array $dictionary;

    public function __construct(array $dictionary)
    {
        $this->dictionary = $dictionary;
    }

    public function getByKey($key): string
    {
        return $this->dictionary[$key];
    }

    public function count(): int
    {
        return count($this->dictionary);
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->dictionary);
    }
}
