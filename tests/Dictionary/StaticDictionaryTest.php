<?php

declare(strict_types=1);

namespace Tests\Craynic\AhoCorasick\Dictionary;

use Craynic\AhoCorasick\Dictionary\StaticDictionary;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class StaticDictionaryTest extends TestCase
{
    private const DICTIONARY = [
        'foo' => 'foo',
        1 => 'bar',
        'xxx' => 'test',
        'bar' => 'the',
        'confusing' => 'best',
    ];

    public function testCountWorks(): void
    {
        $dictionary = new StaticDictionary(self::DICTIONARY);

        $this->assertCount(5, $dictionary);
    }

    public function testIteratorWorks(): void
    {
        $dictionary = new StaticDictionary(self::DICTIONARY);

        $this->assertSame(self::DICTIONARY, iterator_to_array($dictionary));
    }

    #[DataProvider('getByKeyWorksDataProvider')]
    public function testGetByKeyWorks(mixed $key, string $item): void
    {
        $dictionary = new StaticDictionary(self::DICTIONARY);

        $this->assertSame($item, $dictionary->getByKey($key));
    }

    public static function getByKeyWorksDataProvider(): Generator
    {
        yield ['foo', 'foo'];
        yield [1, 'bar'];
    }
}
