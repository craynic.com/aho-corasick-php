<?php
declare(strict_types=1);

namespace Tests\Craynic\AhoCorasick;

use Craynic\AhoCorasick\Dictionary\StaticDictionary;
use Craynic\AhoCorasick\Replacer;
use Craynic\AhoCorasick\SearchTree\AhoCorasickSearchTree;
use Craynic\AhoCorasick\SearchTree\RegExpSearchTree;
use Craynic\AhoCorasick\SearchTree\SearchTree;
use Craynic\AhoCorasick\Utf8Iterator\NaiveUtf8Iterator;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class ReplacerTest extends TestCase
{
    /**
     * @param string[] $replaces
     */
    #[DataProvider('itWorksDataProvider')]
    public function testItWorks(SearchTree $searchTree, array $replaces, string $text, string $expected): void
    {
        $searches = new StaticDictionary(array_keys($replaces));
        $replaces = new StaticDictionary(array_values($replaces));

        $searchTree->setDictionary($searches);

        $replace = new Replacer($searchTree);

        $this->assertSame(
            $expected,
            $replace->replace($text, $replaces)
        );
    }

    public static function itWorksDataProvider(): Generator
    {
        $acTree = new AhoCorasickSearchTree(new NaiveUtf8Iterator());
        $reTree = new RegExpSearchTree();

        foreach (self::itWorksTestCasesProvider() as $testCase) {
            yield array_merge([$acTree], $testCase);
            yield array_merge([$reTree], $testCase);
        }
    }

    private static function itWorksTestCasesProvider(): Generator
    {
        $replaces = ['test' => 'best', 'foo' => 'bar'];
        $text = 'This is a test of a foo.';
        $expected = 'This is a best of a bar.';

        yield [$replaces, $text, $expected];

        $replaces = [
            'test' => 'X',
            'foo' => 'B',
            'x' => '___masked "X" letter___',
            'e' => '___masked "E" letter___'
        ];
        $text = 'This is a x-test of a foo.';
        $expected = 'This is a ___masked "X" letter___-X of a B.';

        yield [$replaces, $text, $expected];
    }}
