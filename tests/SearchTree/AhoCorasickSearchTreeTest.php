<?php declare(strict_types=1);

namespace Tests\Craynic\AhoCorasick\SearchTree;

use Craynic\AhoCorasick\Dictionary\StaticDictionary;
use Craynic\AhoCorasick\SearchTree\AhoCorasickSearchTree;
use Craynic\AhoCorasick\SearchTree\MatchToken;
use Craynic\AhoCorasick\Utf8Iterator\NaiveUtf8Iterator;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;

final class AhoCorasickSearchTreeTest extends SearchTreeTestCase
{
    private const TEST_DICTIONARY = [
        'key-foo'   => 'foo',
        'key-bar'   => 'bar',
        'key-fo'    => 'fo',
        'key-argon' => 'argon',
        'key-FO'    => 'FO',
    ];

    #[DataProvider('searchDataProvider')]
    public function testSearch(string $subject, array $expectedResult): void
    {
        $searchTree = new AhoCorasickSearchTree(new NaiveUtf8Iterator());
        $this->setUpSearchTree($searchTree, self::TEST_DICTIONARY);

        $this->assertSearchTreeResult(
            $expectedResult,
            $searchTree->search($subject)
        );
    }

    public static function searchDataProvider(): Generator
    {
        yield [
            'this tēxt contains foo and bar in one sentence',
            [
                new MatchToken('key-fo', 20, 21, 19, 20),
                new MatchToken('key-foo', 20, 22, 19, 21),
                new MatchToken('key-bar', 28, 30, 27, 29),
            ]
        ];
    }

    #[DataProvider('searchCaseInsensitiveDataProvider')]
    public function testSearchCaseInsensitive(string $subject, array $expectedResult): void
    {
        $searchTree = new AhoCorasickSearchTree(new NaiveUtf8Iterator(), AhoCorasickSearchTree::IGNORE_CASE);
        $this->setUpSearchTree($searchTree, self::TEST_DICTIONARY);

        $this->assertSearchTreeResult($expectedResult, $searchTree->search($subject));
    }

    public static function searchCaseInsensitiveDataProvider(): Generator
    {
        yield [
            'this tēxt contains FOO and BAR in one sentence',
            [
                new MatchToken('key-fo', 20, 21, 19, 20),
                new MatchToken('key-FO', 20, 21, 19, 20),
                new MatchToken('key-foo', 20, 22, 19, 21),
                new MatchToken('key-bar', 28, 30, 27, 29),
            ]
        ];
    }

    public function testHugeDictionary(): void
    {
        $needle = 'test_the_best';
        $needleSuffixLength = 10000;
        $needlePrefixLength = 10000;
        $start = 1000000000;
        $count = 10000;
        $words = [$start - 1 => $needle];

        for ($offset = 0; $offset < $count; $offset++) {
            $words[$start + $offset] = (string) ($start + $offset);
        }

        $searchedText = $this->generateRandomString($needlePrefixLength)
            . $needle
            . $this->generateRandomString($needleSuffixLength);

        $searchTree = new AhoCorasickSearchTree(new NaiveUtf8Iterator());
        $searchTree->setDictionary(new StaticDictionary($words));

        $this->assertSearchTreeResult(
            [new MatchToken(
                $start - 1,
                $needlePrefixLength,
                $needlePrefixLength + mb_strlen($needle) - 1,
                $needlePrefixLength,
                $needlePrefixLength + strlen($needle) - 1
            )],
            $searchTree->search($searchedText)
        );
    }

    private function generateRandomString(int $length): string
    {
        $spool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $nonRandomString = str_repeat($spool, (int) ceil($length / strlen($spool)));
        $randomString = str_shuffle($nonRandomString);

        return substr($randomString, 0, $length);
    }
}
