<?php declare(strict_types=1);

namespace Tests\Craynic\AhoCorasick\SearchTree;

use Craynic\AhoCorasick\Dictionary\StaticDictionary;
use Craynic\AhoCorasick\SearchTree\MatchToken;
use Craynic\AhoCorasick\SearchTree\SearchTree;
use Generator;
use PHPUnit\Framework\TestCase;

abstract class SearchTreeTestCase extends TestCase
{
    protected function setUpSearchTree(SearchTree $searchTree, array $dictionary): void
    {
        $searchTree->setDictionary(new StaticDictionary($dictionary));
    }

    protected function assertSearchTreeResult(array $expectedResult, Generator $result, string $message = ''): void
    {
        $actualResult = iterator_to_array($result);

        $this->sortSearchTreeResult($expectedResult);
        $this->sortSearchTreeResult($actualResult);

        $this->assertEquals($expectedResult, $actualResult, $message);
    }

    private function sortSearchTreeResult(array &$searchTreeResult): void
    {
        usort(
            $searchTreeResult,
            function (MatchToken $matchA, MatchToken $matchB): int {
                return strcmp($matchA->getMatchKey(), $matchB->getMatchKey());
            }
        );
    }
}
