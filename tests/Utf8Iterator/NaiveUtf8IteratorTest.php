<?php declare(strict_types=1);

namespace Tests\Craynic\AhoCorasick\Utf8Iterator;

use Craynic\AhoCorasick\Utf8Iterator\NaiveUtf8Iterator;
use Generator;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

final class NaiveUtf8IteratorTest extends TestCase
{
    #[DataProvider('dataProvider')]
    public function test(string $string): void
    {
        $expectedResult = preg_split('/(?<!^)(?!$)/u', $string);
        $actualResult = iterator_to_array((new NaiveUtf8Iterator())->iterate($string));

        $this->assertSame($expectedResult, $actualResult);
    }

    public static function dataProvider(): Generator
    {
        yield ['This is a test UTF-8 string with ASCII characters only'];
        yield ['+ěščřžýáíé§ąėę∂ßł€'];
        yield ['ह€한𐍈'];
    }
}
